#!/bin/bash

# SPDX-license-identifier: Apache-2.0
##############################################################################
# Copyright (c) 2018 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0
##############################################################################

set -o errexit
set -o nounset
set -o pipefail

labels=$*
RUN_SCRIPT=${0}
RUN_ROOT=$(dirname $(readlink -f ${RUN_SCRIPT}))
export RUN_ROOT=$RUN_ROOT
source ${RUN_ROOT}/scripts/chained-ci-tools/rc.sh
trap submit_bug_report ERR

#-------------------------------------------------------------------------------
# If no labels are set with args, run all
#-------------------------------------------------------------------------------
if [[ $labels = "" ]]; then
  labels="prepare configure deploy postconfiguration"
fi

if [ -v DONT_PIPELINE ]; then
  if [ ${DONT_PIPELINE} = "true" ]; then
    step_banner "Prepare ansible.cfg"
    sed -i 's/pipelining = True/pipelining = False/' ansible.cfg
    sed -i '/^ssh_args/d' ansible.cfg
  fi
fi

if [[ $labels = *"prepare"* ]]; then
  #-------------------------------------------------------------------------------
  # Prepare servers
  #  - add needed packages and prepare the servers
  #-------------------------------------------------------------------------------
  step_banner "Prepare servers"
  step_line "Fetch galaxy roles"
  ansible-galaxy install -r requirements.yml
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/opnfv-k8s-prepare.yaml  \
    --vault-id ${RUN_ROOT}/.vault

  step_banner "Servers prepared"
fi

if [[ $labels = *"configure"* ]]; then
  #-------------------------------------------------------------------------------
  # Configure kubespray servers
  #  - retrieve kubespray
  #  - configure kubespray
  #-------------------------------------------------------------------------------
  step_banner "Configure Kubespray"
  if [ -f ${RUN_ROOT}/vars/openstack_user_openrc ]; then
    ## ONLY NEEDED TO SAY IT EXISTS
    ## BETTER WAY TO DO IT?
    echo "using openstack credentials"
    VAULTED=$(cat ${RUN_ROOT}/vars/openstack_user_openrc | grep -c '^$ANSIBLE_VAULT' || true)
    if [[ ${VAULTED} = "1" ]]; then
      echo "decrypting credentials"
      ansible-vault decrypt --vault-password-file .vault ${RUN_ROOT}/vars/openstack_user_openrc
    fi
    source ${RUN_ROOT}/vars/openstack_user_openrc
  fi
  ansible-galaxy install -r requirements.yml
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/opnfv-k8s-configure.yaml  \
    --vault-id ${RUN_ROOT}/.vault

  step_banner "Kubespray prepared"
fi

#-------------------------------------------------------------------------------
# Install Kubernetes
#  - install kubernetes
#  - install helm
#-------------------------------------------------------------------------------
if [[ $labels = *"deploy"* ]]; then
  step_banner "Install Kubernetes"
  VAULTED=$(cat ${RUN_ROOT}/inventory/infra | grep -c '^$ANSIBLE_VAULT' || true)
  if [[ ${VAULTED} = "1" ]]; then
    echo "decrypting inventory"
    ansible-vault decrypt --vault-password-file .vault --output \
      ${RUN_ROOT}/kubespray_inventory/infra ${RUN_ROOT}/inventory/infra
  else
    cp ${RUN_ROOT}/inventory/infra ${RUN_ROOT}/kubespray_inventory/infra
  #ansible -i kubespray_inventory/infra -m debug -a 'msg="{{ hostvars }}' all
  fi
  cd kubespray
  virtualenv kube
  set +u
  source kube/bin/activate
  set -u
  pip install -r requirements.txt
  # Check if artifacts gave us openrc
  if [ -f ${RUN_ROOT}/vars/openstack_user_openrc ]; then
    echo "using openstack credentials"
    VAULTED=$(cat ${RUN_ROOT}/vars/openstack_user_openrc | grep -c '^$ANSIBLE_VAULT' || true)
    if [[ ${VAULTED} = "1" ]]; then
      echo "decrypting credentials"
      ansible-vault decrypt --vault-password-file ${RUN_ROOT}/.vault ${RUN_ROOT}/vars/openstack_user_openrc
    fi
    source ${RUN_ROOT}/vars/openstack_user_openrc
  fi
  if [ -f ${RUN_ROOT}/vars/ca.pem ]; then
    echo "using given ca.pem"
    mkdir -p ${RUN_ROOT}/kubespray/vars
    VAULTED=$(cat ${RUN_ROOT}/vars/ca.pem | grep -c '^$ANSIBLE_VAULT' || true)
    if [[ ${VAULTED} = "1" ]]; then
      echo "decrypting certificate"
      ansible-vault decrypt --vault-password-file ${RUN_ROOT}/.vault ${RUN_ROOT}/vars/ca.pem
    fi
    cp ${RUN_ROOT}/vars/ca.pem ${RUN_ROOT}/kubespray/vars/ca.pem
  fi
  export ANSIBLE_INVALID_TASK_ATTRIBUTE_FAILED=False
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/kubespray_inventory/infra \
    -b cluster.yml --vault-id ${RUN_ROOT}/.vault
  cd ${RUN_ROOT}
  step_banner "Kubernetes installed"

fi

#-------------------------------------------------------------------------------
# Monitor Kubernetes
#-------------------------------------------------------------------------------
if [[ $labels = *"monitoring"* ]]; then
  step_banner "Kubernetes monitoring"
  step_line "Fetch galaxy roles"
  ansible-galaxy install -r requirements.yml
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/opnfv-k8s-monitoring.yaml  \
    --vault-id ${RUN_ROOT}/.vault
    step_banner "Kubernetes under monitoring"
fi

#-------------------------------------------------------------------------------
# postconfigure Kubernetes
#  - retrieve kubernetes credentials
#-------------------------------------------------------------------------------
if [[ $labels = *"postconfiguration"* ]]; then
  step_banner "Kubernetes post configuration"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/opnfv-k8s-postconfigure.yaml  \
    --vault-id ${RUN_ROOT}/.vault
    step_banner "Kubernetes post configured"
fi
