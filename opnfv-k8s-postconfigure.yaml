---
- hosts: kube-master
  vars_files:
    - "vars/idf.yml"
    - "vars/pdf.yml"
  tasks:
    - block:
        - name: copy kube configuration
          become: "yes"
          copy:
            dest: /tmp/kube-config
            remote_src: "yes"
            src: /etc/kubernetes/admin.conf
            mode: 'u=rw'
            owner: "{{ ansible_user_id }}"

        - name: ensure .kube directory exists
          file:
            path: "{{ ansible_user_dir }}/.kube"
            state: directory

        - name: copy kube/config
          copy:
            src: /tmp/kube-config
            remote_src: "yes"
            dest: "{{ ansible_user_dir }}/.kube/config"

        - name: change IP address
          replace:
            path: /tmp/kube-config
            regexp: "{{ ip }}:"
            replace: "{{ public_ip }}:"
          when: ip is defined and public_ip is defined

        - name: retrieve kube configuration
          fetch:
            dest: "{{ playbook_dir }}/vars/kube-config"
            src: /tmp/kube-config
            flat: "yes"
          run_once: "yes"

      always:
        - name: delete tmp file
          file:
            path: /tmp/kube-config
            state: absent
          become: "yes"

    - name: get helm latest release
      get_url:
        url:
          https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get
        dest: /tmp/get_helm.sh
        force: "yes"
        mode: 0700

    - name: "install helm {{ helm_release }}"
      command: "/tmp/get_helm.sh"
      environment:
        DESIRED_VERSION: "{{ helm_release }}"
      when: "True"

    - name: configure helm2 stuff
      block:
        - name: get helm latest release
          get_url:
            url:
              https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get
            dest: /tmp/get_helm.sh
            force: "yes"
            mode: 0700

        - name: "install helm {{ helm_release }}"
          command: "/tmp/get_helm.sh"
          environment:
            DESIRED_VERSION: "{{ helm_release }}"
          when: "True"

        - name: create tiller service account
          run_once: "yes"
          k8s:
            state: present
            definition:
              apiVersion: v1
              kind: ServiceAccount
              metadata:
                name: tiller
                namespace: kube-system

        - name: create tiller clusterrolebinfing
          run_once: "yes"
          k8s:
            state: present
            definition:
              apiVersion: rbac.authorization.k8s.io/v1
              kind: ClusterRoleBinding
              metadata:
                name: tiller
              roleRef:
                apiGroup: rbac.authorization.k8s.io
                kind: ClusterRole
                name: cluster-admin
              subjects:
                - kind: ServiceAccount
                  name: tiller
                  namespace: kube-system

        - name: init helm and tiller (one time)
          run_once: "yes"
          command: >
            helm init --service-account tiller
            --stable-repo-url=https://charts.helm.sh/stable
          changed_when: "True"

        - name: init helm for everybody
          command: >
            helm init --client-only
            --stable-repo-url=https://charts.helm.sh/stable
          changed_when: "True"
      when: helm_release is version('v3', '<')

    - block:
        - name: create etc metallb configuration directory
          become: "yes"
          file:
            path: "{{ etc_metallb_dir }}"
            state: directory

        - name: generate metallb configuration
          become: "yes"
          copy:
            dest: "{{ etc_metallb_dir }}/metallb-overrides.yaml"
            content: |
              configInline:
                address-pools:
                  - name: default
                    protocol: layer2
                    addresses:
                      - {{ (xci.net_config.admin.network ~ '/' ~
                            xci.net_config.admin.mask) | ipaddr('net') |
                            ipaddr('-30') | ipaddr('address') }}-{{
                              (xci.net_config.admin.network ~ '/' ~
                                xci.net_config.admin.mask) |
                                ipaddr('net') | ipaddr('-2') |
                                ipaddr('address') }}
        - name: install metallb
          command:
            "helm install --name metallb -f \
              {{ etc_metallb_dir }}/metallb-overrides.yaml stable/metallb"
          run_once: "yes"
      when:
        (nodes | selectattr('node.type', 'eq', 'baremetal') | list | length) > 0
